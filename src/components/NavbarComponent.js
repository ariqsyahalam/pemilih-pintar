// Dalam file src/components/NavbarComponent.js

import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const NavbarComponent = () => {
    return (
        <header>
            <p className='text-blue-500'>PemilihPintar.id</p>
        </header>
    );
};

export default NavbarComponent;
