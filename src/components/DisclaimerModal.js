// Dalam file src/components/DisclaimerModal.js

import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const DisclaimerModal = ({ show, onAgree }) => {
    return (
        <Modal
            show={show}
            onHide={() => { }} // Kita biarkan fungsi kosong untuk onHide agar tidak melakukan apa-apa
            backdrop="static" // Ini mencegah pengguna menutup modal dengan mengklik di luar modal
            keyboard={false}  // Ini mencegah pengguna menutup modal dengan tombol escape
            centered         // Ini akan memusatkan modal secara vertikal di layar
        >
            <Modal.Header>
                <Modal.Title>Penafian</Modal.Title>
            </Modal.Header>
            <Modal.Body className='max-sm:text-xs'>
                <p className="whitespace-pre text-wrap">Soal-soal dalam kuis ini dihasilkan melalui pengolahan kecerdasan buatan (AI) berdasarkan program kerja masing-masing calon dan berbagai sumber lainnya. Pengembang website ini tidak bertanggung jawab sepenuhnya atas konten yang disajikan dan bertujuan hanya sebagai sarana untuk mengenalkan pemilih kepada para calon. Kami menganjurkan para pengunjung untuk membaca program kerja masing-masing calon secara langsung guna memperoleh informasi yang lebih akurat dan lengkap.</p>
                <br></br>
                <p className="whitespace-pre text-wrap">Kami menghargai privasi Anda. Aplikasi ini tidak mengumpulkan atau menyimpan data pribadi pengguna. Semua jawaban dan interaksi hanya disimpan pada perangkat Anda dan tidak dibagikan dengan pihak ketiga.</p>
                <br></br>
                <p className="whitespace-pre text-wrap">Silakan menekan 'Saya Mengerti' untuk setuju dan melanjutkan.</p>
            </Modal.Body>
            <Modal.Footer>
                <button className="btn btn-primary w-100" onClick={onAgree}>
                    Saya Mengerti
                </button>
            </Modal.Footer>
        </Modal>
    );
};

export default DisclaimerModal;
